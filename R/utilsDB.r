



#'(data base) Create a connection to an MS Access database using ODBC
#'
#' @param db_path Path and filename to an MS Access database
#'
#' @return A ODBC connection to an MS Access databases
#'
#' @importFrom odbc odbc
#'
#' @export
#'
connectAccessDb <- function(db_path) {
  driverName <- "Driver={Microsoft Access Driver (*.mdb, *.accdb)};"
  driverName <- paste0(driverName, "DBQ=", normalizePath(db_path))

  cn <- odbc::dbConnect(odbc::odbc(), .connection_string = driverName)
  cn
}#END connectAccessDb







#' Query any data base
#'
#' @param path A character vector, length one. File path to the data base.
#' @param sql A character vector, length one. The SQL statement.
#'
#' @return A data frame based on the SQL
#' @export
#'
#' @importFrom odbc dbConnect dbGetQuery dbDisconnect
#'
#' @examples
#' \dontrun{
#'
#' filepath <- "C:/CASClient_2020_BE.mdb"
#' sql <- "SELECT CFileFishery.* FROM CFileFishery;"
#' df <- querydbase(path=filepath, sql=sql)
#' str(df)
#' }
querydbase <- function(path=NA, sql=NA){

  # driverName <- "Driver={Microsoft Access Driver (*.mdb, *.accdb)};"
  # driverName <- paste0(driverName, "DBQ=", path)
  #
  # cn <- odbc::dbConnect(odbc::odbc(), .connection_string=driverName)
  cn <- connectAccessDb(path)
  results <- odbc::dbGetQuery(cn, sql )

  odbc::dbDisconnect(cn)

  return(results)

}#END querydbase


