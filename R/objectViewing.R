
more <- function(x, n=10){
	start <- seq(1, by=n, length.out = ceiling(length(x)/n))
	end <- start+(n-1)
	end[length(end)] <- length(x)
	
	for(i in 1:length(start)){
		cat(paste(x[start[i]:end[i]], collapse = "\n"))
		if(i<length(start)) readline(prompt="Press [enter] to continue")
	}
}#END more
