



#' @title Build a data frame comprising all functions found in R files
#'
#' @param path A character vector of length one. Folder location to search for R
#'   files.
#' @param filename.pattern A character vector of length one. The pattern used in
#'   the search for R files. Default is '\\.R$'.
#' @param recursive A logical. Argument to \code{\link{list.files}}, default is
#'   TRUE.
#'
#' @return A data frame of function names and the filepath where each can be
#'   found.
#' @export
#'
#' @details
#'
#' @examples
#' \dontrun{
#' listFunctions()
#' res <- listFunctions()
#' View(res)
#' }
listFunctions <- function(path=".", filename.pattern = "\\.R$", recursive=TRUE,...){

  #idea taken from: https://stackoverflow.com/questions/39815780/r-list-functions-in-file

  is_function = function (expr) {
    if (! is_assign(expr))
      return(FALSE)
    value = expr[[3]]
    is.call(value) && as.character(value[[1]]) == 'function'
  }

  function.name = function (expr)
    as.character(expr[[2]])

  is_assign = function (expr)
    is.call(expr) && as.character(expr[[1]]) %in% c('=', '<-', 'assign')

  R.filenames <- list.files(path = path, pattern = filename.pattern, ignore.case = TRUE, recursive = recursive, full.names = TRUE,...)

  res <- lapply(R.filenames, function(filename){
    file_parsed <- parse(filename)
    functions <- Filter(is_function, file_parsed)
    function.names <- unlist(Map(function.name, functions))
    data.frame(function.names=function.names, filename=filename, stringsAsFactors = FALSE)
  })
  res <- do.call(rbind, res)
  res <- res[order(res$function.names),]

  return(res)
}#END listFunctions





listFunctions.old <- function(filename.pattern = "_functions\\.R$"){
	filepaths <- list.files( pattern = filename.pattern, ignore.case = TRUE, recursive = TRUE)
	#filepaths <- list.files( pattern = "\\.R$", ignore.case = TRUE, recursive = TRUE)
	#filepaths <- filepaths[-grep(pattern = "_OFF", filepaths)]

	results <- lapply(filepaths, function(x){
		source(x, local = TRUE)
		fun.names <- lsf.str()
		fun.list <- lapply(fun.names, function(function.name){
			list(filepath=x, function.name=function.name, args=formals(function.name), body=functionBody(function.name), res.get=get(function.name))
		})
		return(fun.list)
	})
	results <- do.call("c", results)
	return(results)
}#END listFunctions




#' @title Search for packages being loaded in R files
#'
#' @param path A character vector of length one. Folder location to search for R
#'   files.
#' @param filename.pattern A character vector of length one. The pattern used in
#'   the search for R files. Default is '\\.R$'.
#' @param recursive A logical. Argument to \code{\link{list.files}}, default is
#'   TRUE.
#'
#' @return A vector of package names.
#' @export
#'
#' @description
#'
#' @examples
#' \dontrun{
#' listPackages()
#' }
listPackages <- function(path=".", filename.pattern= "\\.R$", recursive=TRUE){

	R.filenames <- list.files(path = path, pattern = filename.pattern, ignore.case = TRUE, recursive = recursive)
	fn.name <- c("usePackage\\(", "require\\(", "library\\(", "load_or_install\\(" )

	results <- lapply(R.filenames, function(x, fn.name){
		rfile <- readLines(x)
		res.tmp <- unique(grep(paste(fn.name, collapse = "|"), rfile, value = TRUE))
		res.tmp <- lapply(res.tmp, function(x2){
			start.ind <- unlist(gregexpr(pattern = "\\\"", x2))
			x2 <- substring(x2, start.ind[1:(length(start.ind)-1)]+1,  start.ind[2:(length(start.ind))]-1)
			x2 <- x2[x2 != ","]
			x2
		})
		res.tmp <- c(do.call("rbind", res.tmp))
		return(res.tmp)
	}, fn.name)


	results <- do.call("rbind", results)
	results <- unique(sort(trimws(results)))
	results
}#END listPackages


#' @title Package load or install and load
#'
#' @description For installing and/or loading R packages
#'
#' @param package_names Character string of length one or more. The package name.
#'
#' @return Nothing returned.
#' @export
#'
#' @examples
load_or_install <- function(package_names){
	# function to load/install required packages
	for(package_name in package_names){
		if(!is_installed(package_name)){install.packages(package_name,repos="http://lib.stat.cmu.edu/R/CRAN")}
		library(package_name,character.only=TRUE,quietly=TRUE,verbose=FALSE)
	}
}#END load_or_install






#' Source all R files in a folder.
#'
#' @param path
#' @param filename.pattern
#' @param ignore.case
#' @param ...
#'
#' @return
#' @export
#'
#' @examples
sourceFolder <- function(path=NA, filename.pattern="\\.R$", ignore.case=TRUE,...){

  R.filenames <- list.files(path, pattern = filename.pattern, full.names = TRUE, ignore.case = ignore.case, ...)
  lapply(R.filenames, source)

}#END sourceFolder


