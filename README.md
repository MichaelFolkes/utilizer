# R package: utilizer 
A suite of R functions for some basic tasks.

If you are using Ubuntu/Linux, for a successful install of devtools, you may need to run this line in BASH:

`sudo apt-get install libcurl4-openssl-dev libssl-dev`

Then in R:
    
```{r} 
install.packages("remotes") 
remotes::install_gitlab("MichaelFolkes/utilizer") 
```

